const express = require('express');
const router = express.Router();


const createRouter = connection => {

    router.get('/', (req, res) => {

        connection.query('SELECT * FROM `comment`', (error, result) => {
            if (error) {
                res.status(500).send({error: "Comments error"})
            }

            res.send(result)
        });

    });

    router.get('/:id', (req, res) => {
        connection.query('SELECT * FROM `comment` WHERE `news_id` = ?', req.params.id, (error, result) => {
            if (error) {
                res.status(500).send({error: "Category id is error"})
            }
            res.send(result);

        });
    });

    router.post('/:id', (req, res) => {

        const commentBody = req.body;

        if (!commentBody.author) {
            commentBody.author = "anonymous"
        }

        connection.query('INSERT INTO `comment`  (`news_id`, `author`, `comment`) VALUES (?, ?, ?)',
            [commentBody.news_id, commentBody.author, commentBody.comment],
            (error, result) => {
                if (error) {
                    res.status(500).send({error: "Error"})
                }

                res.send(commentBody)
            });

    });

    router.delete('/:id', (req, res) => {
        connection.query('DELETE FROM `comment` WHERE `id` = ?', req.params.id, (error) => {
            if (error) {
                res.status(500).send({message: error.sqlMessage})
            }

            res.send({message: "Success"})
        })


    });


    return router
};


module.exports = createRouter;