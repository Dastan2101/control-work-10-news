const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath)
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = connection => {

    router.get('/', (req, res) => {

        connection.query('SELECT * FROM `news`', (error, result) => {
            if (error) {
                res.status(500).send({error: "Database error"})
            }

            const filter = [];

            result.map(item => {
                filter.push({id: item.id, title: item.title, datetime: item.datetime, image: item.image})
            });

            res.send(filter)

        });

    });


    router.post('/', upload.single('image'), (req, res) => {

        const newsInfo = req.body;

        if (req.file) {
            newsInfo.image = req.file.filename;
        }

        let dateTime = new Date().toISOString();

        newsInfo.datetime = dateTime.slice(0, -14) + ' ' + dateTime.slice(11, -5);

        connection.query('INSERT INTO `news` (`title`, `content`, `datetime`, `image`) VALUES (?, ?, ?, ?)',
            [newsInfo.title, newsInfo.content, newsInfo.datetime, newsInfo.image],
            (error, result) => {
                if (error) {
                    res.status(500).send({error: "Couldn't find"})
                }

                res.send(newsInfo)
            });

    });

    router.get('/:id', (req, res) => {
        connection.query('SELECT * FROM `news` WHERE `id` = ?', req.params.id, (error, result) => {
            if (error) {
                res.status(500).send({error: "Oops sorry error"})
            }
            res.send(result[0]);

        });
    });

    router.delete('/:id', (req, res) => {
        connection.query('DELETE FROM `news` WHERE `id` = ?', req.params.id, (error, result) => {
            if (error) {
                res.status(500).send({message: error.sqlMessage})
            }

            res.send({message: "Success deleted news post"})
        })
    });


    return router
};


module.exports = createRouter;