import React, { Component,Fragment } from 'react';
import {Route, Switch} from "react-router-dom";
import Toolbar from "../components/UI/Toolbar/Toolbar";
import {Container} from "reactstrap";
import NewsList from "../components/NewsList/NewsList";
import AddNewPost from "../components/AddNewPost/AddNewPost";
import FullNews from "../components/FullNews/FullNews";

class App extends Component {
    render() {
        return (
            <Fragment>
                <header>
                    <Toolbar/>
                </header>
                <Container>
                    <Switch>
                        <Route path="/" exact component={NewsList} />
                        <Route path="/add" component={AddNewPost} />
                        <Route path="/news/:id" component={FullNews} />
                    </Switch>
                </Container>
            </Fragment>
        );
    }
}

export default App;
