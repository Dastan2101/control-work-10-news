import axios from '../../axios-news';

export const FETCH_NEWS_SUCCESS = 'FETCH_NEWS_SUCCESS';

export const FETCH_FULL_NEWS_INFO = 'FETCH_FULL_NEWS_INFO';

export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';

export const fetchNewsSuccess = data => ({type: FETCH_NEWS_SUCCESS, data});
export const fetchFullNews = data => ({type: FETCH_FULL_NEWS_INFO, data});
export const fetchCommentsSuccess = data => ({type: FETCH_COMMENTS_SUCCESS, data});

export const fetchNews = () => {
    return dispatch => {
        return axios.get('/news').then(
            response => dispatch(fetchNewsSuccess(response.data))
        )
    }
};

export const createPost = data => {
    return dispatch => {
        return axios.post('/news', data).then(
            () => dispatch(fetchNews())
        )
    }
};

export const getFullInfo = id => {
    return dispatch => {
        return axios.get('news/' + id).then(
            response => dispatch(fetchFullNews(response.data))
        )
    }
};

export const getComments = (id) => {
    return dispatch => {
        return axios.get('/comment/' + id).then(
            response => dispatch(fetchCommentsSuccess(response.data))
        )
    }
};

export const SendComments = (id, data) => {
    return dispatch => {
        return axios.post('/comment/' + id + '/', data).then(
            () => dispatch(getComments(id))
        )
    }
};

export const deleteComment = (id) => {
        return axios.delete('/comment/' + id).then(
            window.location.reload()
        )
};

export const deletePost = id => {
    return dispatch => {
        return axios.delete('/news/' + id).then(
            () => dispatch(fetchNews())
        )
    }
};