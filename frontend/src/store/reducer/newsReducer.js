import {FETCH_COMMENTS_SUCCESS, FETCH_FULL_NEWS_INFO, FETCH_NEWS_SUCCESS} from "../actions/Actions";

const initialState = {
    newsList : null,
    fullInfo: {},
    comments: null,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_NEWS_SUCCESS:
            return {...state, newsList: action.data};
        case FETCH_FULL_NEWS_INFO:
            return {...state, fullInfo: action.data};
        case FETCH_COMMENTS_SUCCESS:
            return {...state, comments: action.data};
        default:
            return state;
    }
};

export default reducer;