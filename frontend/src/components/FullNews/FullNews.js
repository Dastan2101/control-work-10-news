import React, {Component, Fragment} from 'react';
import {deleteComment, getComments, getFullInfo, SendComments} from "../../store/actions/Actions";
import {connect} from "react-redux";
import {Button, Col, Form, FormGroup, Input, Label, Spinner} from "reactstrap";

const styleComment = {
    padding: '10px',
    width: '30%',
    border: '1px solid #c5c8cc',
    backgroundColor: '#cccc',
    margin: '10px 0',
};

const styleDelete = {
    display: 'table',
    margin: '3px 5px auto auto',
    cursor: 'pointer'
};

class FullNews extends Component {

    state = {
        name: '',
        comment: ''
    };

    componentDidMount() {
        let id = this.props.match.params.id;
        this.props.getPost(id);
        this.props.getComments(id);

    }

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    sendComment = () => {
        let id = this.props.match.params.id;
        if (this.state.comment !== '') {
            let data = {
                author: this.state.name !== '' ? this.state.name : null,
                comment: this.state.comment,
                news_id: id
            };
            this.props.SendComments(id, data);
        }

        this.props.getComments(id);


    };


    removeComment = id => {
        this.props.deleteComment(id);
        this.props.getComments(this.props.match.params.id);
    };

    render() {
        let comments;
        let information = this.props.fullInfo;
        if (!this.props.comments) {
            return comments = <Spinner/>
        } else {
            comments = this.props.comments.map(item => (
                <div style={styleComment} key={item.id}>
                    <p>author: <i>{item.author}</i></p>
                    <p>{item.comment}</p>
                    <p style={styleDelete} onClick={() => this.removeComment(item.id)}>&#10060;</p>
                </div>
            ))
        }

        return (
            <Fragment>
                <div>
                    <h3>{information.title}</h3>
                    <p>date: {information.datetime}</p>
                    <p><em>{information.content}</em></p>
                </div>
                <h4>Comments :</h4>
                <div className="comments"
                     style={{margin: '50px 0'}}>
                    {comments}
                </div>
                <div>
                    <Form>
                        <h4>Add comment</h4>
                        <FormGroup row>
                            <Label sm={2} for="name">Name</Label>
                            <Col sm={4}>
                                <Input
                                    type="text"
                                    name="name" id="name"
                                    value={this.state.name}
                                    onChange={this.inputChangeHandler}
                                />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={2} for="comment">Comment</Label>
                            <Col sm={4}>
                                <Input
                                    type="textarea" required
                                    name="comment" id="comment"
                                    value={this.state.comment}
                                    onChange={this.inputChangeHandler}
                                />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col sm={{offset: 2, size: 10}}>
                                <Button type="button" color="primary" onClick={this.sendComment}>Add</Button>
                            </Col>
                        </FormGroup>
                    </Form>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    fullInfo: state.news.fullInfo,
    comments: state.news.comments,
});

const mapDispatchToProps = dispatch => ({
    getPost: id => dispatch(getFullInfo(id)),
    getComments: id => dispatch(getComments(id)),
    SendComments: (id, data) => dispatch(SendComments(id, data)),
    deleteComment: id => dispatch(deleteComment(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(FullNews);