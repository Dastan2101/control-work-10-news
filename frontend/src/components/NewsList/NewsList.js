import React, {Component, Fragment} from 'react';
import {Button, Card, CardBody, Spinner} from "reactstrap";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import NewsThumbnail from "../NewsThumbnail/NewsThumbnail";
import {deletePost, fetchNews} from "../../store/actions/Actions";

const styleTitle = {
    margin: '0 10px',
    fontSize: '25px',
    color: '#22788e',
    fontWeight: 'bold'
};

const styleLink = {
    display: 'block',
    textAlign: 'center',
    fontSize: '22px',
    color: '#463790',
    fontWeight: 'bold'
};

class NewsList extends Component {

    componentDidMount() {
        this.props.fetchNews();
    }


    render() {
        let list;
        let addpost;
        if (!this.props.newsList) {
            list = <Spinner/>
        } else {
            list = this.props.newsList.map(item => (
                <Card key={item.id} style={{marginBottom: '10px'}}>
                    <CardBody>
                        <NewsThumbnail image={item.image}/>
                        <p style={styleTitle}>
                            {item.title}
                        </p>
                        <Link
                            to={'/news/' + item.id}
                            style={styleLink}
                        >
                            Read Full Post &#187;
                        </Link>
                        <p style={{margin: '10px', display: 'inline-block'}}>
                            release date: <em>{item.datetime}</em>
                        </p>
                        <Button style={{float: 'right'}} color="danger"
                                onClick={() => this.props.deletePost(item.id)}>Delete</Button>

                    </CardBody>
                </Card>
            ));
            addpost = <Link
                to="/add"
            >
                <Button
                    color="success"
                    className="float-lg-right"
                >
                    Add new post
                </Button>
            </Link>
        }
        return (
            <Fragment>
                <h2>
                    News
                    {addpost}
                </h2>
                {list}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    newsList: state.news.newsList
});

const mapDispatchToProps = dispatch => ({
    fetchNews: () => dispatch(fetchNews()),
    deletePost: (id) => dispatch(deletePost(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(NewsList);