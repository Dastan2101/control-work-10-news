import React from 'react';
import {Nav, Navbar, NavbarBrand} from "reactstrap";
import {NavLink as RouterNavLink} from 'react-router-dom';


const Toolbar = () => {
    return (
        <Navbar color="light" light expand="md">
            <NavbarBrand tag={RouterNavLink} to="/">News List</NavbarBrand>
            <Nav className="ml-auto" navbar>
            </Nav>
        </Navbar>
    );
};

export default Toolbar;